ARG IMAGE=docker.io/php:7-apache

FROM "$IMAGE" AS heurist-install
COPY heurist-install.sh /usr/local/bin/
RUN --mount=type=cache,target=/var/cache/heurist-install heurist-install.sh

FROM "$IMAGE"
RUN --mount=type=bind,from=docker.io/mlocati/php-extension-installer:latest,source=/usr/bin/install-php-extensions,target=/usr/local/bin/install-php-extensions \
    install-php-extensions gd mysqli xsl zip bz2 exif imagick calendar
RUN --mount=type=cache,sharing=locked,target=/var/lib/apt/lists \
    --mount=type=cache,sharing=locked,target=/var/cache/apt \
    rm /etc/apt/apt.conf.d/docker-clean \
    && apt-get update \
    && apt-get --assume-yes --no-install-recommends install mariadb-client
RUN a2enmod rewrite
RUN ln --symbolic /var/run/mysqld/mysqld.sock /tmp/mysql.sock
COPY php.ini /usr/local/etc/php/conf.d/heurist.ini
COPY apache2-conf.conf /etc/apache2/conf-enabled/heurist.conf
COPY apache2-site.conf /etc/apache2/sites-enabled/000-default.conf
COPY --from=heurist-install /var/www/html /var/www/html
