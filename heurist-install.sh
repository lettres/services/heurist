#!/usr/bin/env bash
set -euo pipefail

run() {
    echo + "$@"
    "$@"
}

# cache downloads
wget() {
    local URL="$1"
    local FILENAME="$(basename "$URL")"
    local CACHE=/var/cache/heurist-install/"$FILENAME"
    if ! test -e "$CACHE"
    then curl --fail --location --output "$CACHE" "$URL"
    fi

    ln --symbolic "$CACHE" .
}

# allow these commands to fail
rm() { /usr/bin/env rm "$@" || true; }
chown() { /usr/bin/env chown "$@" || true; }
chgrp() { /usr/bin/env chgrp "$@" || true; }

# fail the build
exit() {
    unset exit
    exit 1
}

wget https://HeuristRef.net/HEURIST/DISTRIBUTION/install_heurist.sh

set -- heurist run
source install_heurist.sh
